<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CashewCLIController
{
    public function index()
    {
        $this->load->library('migration');
        if ( ! $this->migration->current())
        {
            die($this->migration->error_string().PHP_EOL);
        }
        /**
         * Cargar ASSERTS
         */
        $this->load->library('CashewAsserts');
        $this->cashewasserts->load_sql();
        
        /**
         * Cargar datos por defecto si estos no existen.
         */
        $this->cashewasserts->load_models();
        
        echo 'Migrated!'.PHP_EOL;
    }
}
