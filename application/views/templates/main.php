<!doctype html>
<html>
    <head>
        <?php if (isset($title)): ?>
        <title><?php echo $app ?> | <?php echo $title?></title>
        <?php else:?>
        <title><?php echo $app ?></title>
        <?php endif;?>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <?php if(isset($less)):?>
        <?php foreach($less as $file):?>
        <link rel="stylesheet/less" type="text/css" href="<?php echo base_url('/less/'.$file)?>" />
        <?php endforeach;?>
        <?php endif;?>

        <?php if(isset($css)):?>
        <?php foreach($css as $file):?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('/css/'.$file)?>" />
        <?php endforeach;?>
        <?php endif;?>
        
        <?php if(isset($js)):?>
        <?php foreach($js as $file):?>
        <script type="text/javascript" src="<?php echo base_url('/js/'.$file)?>" ></script>
        <?php endforeach;?>
        <?php endif;?>
        <link rel="shortcut icon" href="<?php echo base_url('/favicon.png')?>">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="<?php echo site_url()?>">
                        <?php echo $app?>
                    </a>
                </div>
            </div>
        </div>
        <div class="container cashew-content">
            <div class="row">
                <div class="span12">
                <?php alerts(); ?>
                <?php if (isset($content)) echo $content; ?>
                </div>
            </div>
        </div>
    </body>
</html>