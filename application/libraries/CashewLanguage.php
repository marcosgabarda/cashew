<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Libreria para la internacionalizacion con gettext.
 */
class CashewLanguage
{
    private $CI;

    private $locale;

    private $installed_locales = array();

    /**
     * Inicializa la configuracion de gettext. En primer lugar se evalua un
     * parametro que se puede pasar por GET para forzar el idioma, en segundo
     * lugar, se comprueba si el usuario esta registrado, y si lo esta, se
     * configura el idioma asociado.
     *
     */
    function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->model('User');
        $this->CI->load->library('session');
        $this->CI->load->library('CashewAuth');

        // Configuracion de los idiomas instalados.
        $this->installed_locales = array(
                "es"    => array("name" => _('Español'),
                                 "locale" => 'es_ES'),
                "es_ES" => array("name" => _('Español'),
                                 "locale" => 'es_ES'),
                "en"    => array("name" => _('Ingles'),
                                 "locale" => 'en_US'),
                "en_US" => array("name" => _('Ingles'),
                                 "locale" => 'en_US')
                );
        
        // Comprobamos si por session hay un nuevo cambio de language  
        if(isset($_SESSION['l'])){ 
            $this->set_session_locale($_SESSION['l']);
        }
        
        // Obtencion del idioma pedido. 
        // FIXME: aqui no entra nunca porque siempre tiene session arriba. No es util.
        else if ($this->CI->input->get('l', true) !== false)
        {
            $this->locale = $this->CI->input->get('l', true);
            $this->set_session_locale($this->locale);
        }
            
        // Aqui siempre entra salvo que este forzando. 
        else if(isset($_SESSION['l'])){ 
            $this->set_session_locale($_SESSION['l']);
        }
          
        /*else if ($this->CI->cashewauth->is_authenticated())
        {
            $this->locale = $this->CI->cashewauth->current_user()->locale;
        }*/
        else if ($this->get_session_locale() !== false)
        {
            $this->locale = $this->get_session_locale();
           
        }
        else
        {
            // Valor por defecto.
            $this->locale = $this->installed_locales['es']['locale'];
        } 
        // Configuramos Gettext.
        if (array_key_exists($this->locale, $this->installed_locales))
        {
            $domain = "messages";
            $locale = $this->installed_locales[$this->locale]['locale'].'.UTF-8';
            setlocale(LC_MESSAGES, $locale);
            bindtextdomain($domain, APPPATH."language/locales");
            bind_textdomain_codeset($domain, 'UTF-8');
        }
    }
    
    public function set_session_locale($locale)
    {
        $this->CI->session->set_userdata('locale', $locale);
    }

    public function get_session_locale()
    {
        return $this->CI->session->userdata('locale');
    }
}